package models;

import java.util.*;

import play.data.validation.Constraints.*;

import play.data.*;

import  models.*;
import play.db.ebean.*;
import play.data.validation.Constraints.*;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: Augustin
 * Date: 01/07/13
 * Time: 19:31
 * To change this template use File | Settings | File Templates.
 */

@Entity
public class Task extends Model{

    @Id
    public long id;
    @Required
    public String label;

    public static Finder<Long,Task> find = new Finder(
            Long.class, Task.class
    );

    public static List<Task> all() {
        return find.all();
    }

    public static void create(Task task) {
        task.save();
    }

    public static void delete(Long id) {
        find.ref(id).delete();
    }
}
